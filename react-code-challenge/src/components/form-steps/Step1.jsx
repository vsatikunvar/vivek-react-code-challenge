import React from 'react'



const Step1 = ({onSubmit,handleChange, formData, errors, onSubmitClick }) => {  

  return (
    <div className='step1'>
       <div className="form-group">
       <label htmlFor="first-name">First Name</label>
        <input
            id='first-name'
            name='fname'
            onChange={handleChange}
            value={formData.fname}
          />
          <span className='error-msg'>{errors.errors?.fname}</span>
       </div>
       <div className="form-group">
       <label htmlFor="last-name">Last Name</label>
        <input
            id='last-name'
            name='lname'
            onChange={handleChange}
            value={formData.lname}
          />
          <span className='error-msg'>{errors.errors?.lname}</span>
       </div>
       <div className="form-group">
       <label htmlFor="number">Mobile Number</label>
        <input
            id='number'
            name='number'
            onChange={handleChange}
            value={formData.number}
          />
          <span className='error-msg'>{errors.errors?.number}</span>
       </div>
       <div className="form-group">
       <label htmlFor="email">email</label>
        <input
            id='email'
            name='email'
            onChange={handleChange}
            value={formData.email}
          />
          <span className='error-msg'>{errors.errors?.email}</span>
       </div>
       <button
              type="button"
              onClick={(e)=>onSubmitClick(e,formData, onSubmit)}
              className="btn-primary px-5 py-1 my-2"
            >
              Next
            </button>
    </div>
  )
}

export default Step1
import React from 'react'

const Step2 = ({onSubmit, onBackClick, handleChange, formData, errors, onSubmitClick }) => {
     
  return (
    <div className='step2'>

    <div className="form-group">
        <label htmlFor="department">Department</label>
        <select name="department" value={formData.department}  onChange={handleChange} id='department'>
            {/* <option value={formData.department}>{formData.department}</option> */}
            <option value="" disable></option>
            <option value="FrontEnd">Front End</option>
            <option value="BackEnd">Back End</option>
            <option value="QA">QA</option>
        </select>
        <span className='error-msg'>{errors.errors?.department}</span>
    </div>

    <div className="form-group">
    <label htmlFor="employeeId">EmployeeId</label>
     <input
         id='employeeId'
         name='employeeId'
         onChange={handleChange}
         value={formData.employeeId}
       />
       <span className='error-msg'>{errors.errors?.employeeId}</span>
    </div>

    <div className="form-group">
    <label htmlFor="role">Role</label>
     <input
         id='role'
         name='role'
         onChange={handleChange}
         value={formData.role}
       />
       <span className='error-msg'>{errors.errors?.role}</span>
    </div>
  
   
    <button
           type="button"
           onClick={onBackClick}
           className="btn-primary px-5 py-1 my-2"
         >
           Back
         </button>
    <button
           type="button"
           onClick={(e)=>{onSubmitClick(e,formData, onSubmit)}}
           className="btn-primary px-5 py-1 my-2"
         >
           Next
         </button>
 </div>

  )
}

export default Step2
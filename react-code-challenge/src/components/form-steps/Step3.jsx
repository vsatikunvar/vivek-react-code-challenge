import React from 'react'

const Step3 = ({onSubmit, onBackClick, handleChange, formData, errors, onSubmitClick}) => {
    
   
  return (
     
    <div className='step3'>

    <div className="form-group radio">
        <span>Are you working from Home?
        </span><br />
        <input type="radio" id='yes' name='wfh' value="yes" onChange={handleChange} checked={formData.wfh === "yes"}  />
        <label htmlFor="yes">Yes</label>
        <input type="radio" id='no' name='wfh' value="no" onChange={handleChange}  checked={formData.wfh === "no"} />
        <label htmlFor="no">No</label>
    </div>
        <span className='error-msg'>{errors.errors?.wfh}</span>

    <div className="form-group">
    <label htmlFor="manager">Your Manager</label>
     <input
         id='manager'
         name='manager'
         onChange={handleChange}
         value={formData.manager}
       />
         <span className='error-msg'>{errors.errors?.manager}</span>
    </div>

    <div className="form-group">
   
   <label htmlFor="comment">Add comment</label>
     <textarea
         id='role'
         name='comment'
         onChange={handleChange}
         value={formData.comment}
       />
         
    </div>
  
   
    <button
           type="button"
           onClick={onBackClick}
           className="btn-primary px-5 py-1 my-2"
         >
           Back
         </button>
    <button
           type="button"
           onClick={(e)=>onSubmitClick(e,formData,onSubmit)}
           className="btn-primary px-5 py-1 my-2"
         >
           Submit
         </button>
 </div>
  )
}

export default Step3
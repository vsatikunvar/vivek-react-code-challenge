import React from 'react';
import Popup from '../Popup';
const Step4 = ({step1FormData,step2FormData,step3FormData }) => {
 
    const style = {
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        width: 400,
        bgcolor: 'background.paper',
        border: '2px solid #000',
        boxShadow: 24,
        p: 4,
      };

  return (
    <div className='step4'>
        <span>You have succesfully filled the form. Click below to view form overview.</span> <br />
        <Popup step1FormData={step1FormData} step2FormData={step2FormData} step3FormData={step3FormData}/>

        </div>
  )
}

export default Step4
import React from 'react'

const Accordian = ({step1FormData, step2FormData, step3FormData}) => {
  return (
    <div className="side-panel">
        <h2>Form Overview</h2>
        <div className="accordion" id="accordionExample">
          <div className="accordion-item">
            <h2 className="accordion-header" id="headingOne">
              <button
                className="accordion-button collapsed"
                type="button"
                data-bs-toggle="collapse"
                data-bs-target="#collapseOne"
                aria-expanded="false"
                aria-controls="collapseOne"
              >
                Personal Info
              </button>
            </h2>
            <div
              id="collapseOne"
              className="accordion-collapse collapse"
              aria-labelledby="headingOne"
              data-bs-parent="#accordionExample"
            >
              <div className="accordion-body">
                Fname:{step1FormData.fname} <br />
                Lname:{step1FormData.lname} <br />
                Number:{step1FormData.number}
                <br />
                Email:{step1FormData.email}
                <br />
              </div>
            </div>
          </div>
          <div className="accordion-item">
            <h2 className="accordion-header" id="headingTwo">
              <button
                className="accordion-button collapsed"
                type="button"
                data-bs-toggle="collapse"
                data-bs-target="#collapseTwo"
                aria-expanded="false"
                aria-controls="collapseTwo"
              >
                Employee Info
              </button>
            </h2>
            <div
              id="collapseTwo"
              className="accordion-collapse collapse"
              aria-labelledby="headingTwo"
              data-bs-parent="#accordionExample"
            >
              <div className="accordion-body">
                Department:{step2FormData.department} <br />
                EmployeeId:{step2FormData.employeeId} <br />
                Role:{step2FormData.role}
                <br />
              </div>
            </div>
          </div>
          <div className="accordion-item">
            <h2 className="accordion-header" id="headingThree">
              <button
                className="accordion-button collapsed"
                type="button"
                data-bs-toggle="collapse"
                data-bs-target="#collapseThree"
                aria-expanded="false"
                aria-controls="collapseThree"
              >
                Comments
              </button>
            </h2>
            <div
              id="collapseThree"
              className="accordion-collapse collapse"
              aria-labelledby="headingThree"
              data-bs-parent="#accordionExample"
            >
              <div className="accordion-body">
                WFH:{step3FormData.wfh} <br />
                Manager:{step3FormData.manager} <br />
                comment:{step3FormData.comment}
                <br />
              </div>
            </div>
          </div>
        </div>
      </div>
  )
}

export default Accordian
import React, { lazy, Suspense } from "react";
import { useState } from "react";
import Accordian from "./Accordian";
const Step1 = lazy(() => import("./form-steps/Step1"));
const Step2 = lazy(() => import("./form-steps/Step2"));
const Step3 = lazy(() => import("./form-steps/Step3"));
const Step4 = lazy(() => import("./form-steps/Step4"));

const MainForm = () => {
  const [step, setStep] = useState(1);
  const [step1FormData, setStep1FormData] = useState({
    fname: "",
    lname: "",
    number: "",
    email: "",
  });
  const [step2FormData, setStep2FormData] = useState({
    department: "",
    role: "",
    employeeId: "",
  });
  const [step3FormData, setStep3FormData] = useState({
    comment: "",
    wfh: "",
    manager: "",
  });


  const [errors, setErrors] = useState({
    fname:"",
lname:"",    
number:"",
email:"",
department:"",
role:"",
employeeId:"",
comment: "",
    wfh: "",
    manager: ""
  });

//   form validation function
  const validate = (name, value) => {
    // if(value === '') return `${name} is Required`;
    // else return '';
        
    switch (name) {
      case "fname":
        if (!value || value.trim() === "") {
          return "First name is Required";
        } else {
          return "";
        }
    case "lname":
            if (!value || value.trim() === "") {
              return "Last name is Required";
            } else {
              return "";
            }  
    case "email":
        if (!value) {
          return "Email is Required";
        } 
       else if(!(value.match(
        /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
      ))){
         return "enter correct email format"
       }
        else {
          return "";
        }
      case "number":
        if (!value || value.trim() === "") {
          return "Mobile number is Required";
        }
        
        else if(!(value.match(
          /^(\+\d{1,3}[- ]?)?\d{10}$/ 
        ))){
           return "enter correct mobile format"
         }
         else {
          return "";
        }
        case "department":
            if (!value || value.trim() === "") {
              return "department is Required";
            } else {
              return "";
            }
         case "role":
                if (!value || value.trim() === "") {
                  return "role is Required";
                } else {
                  return "";
                }    
         case "employeeId":
                if (!value || value.trim() === "") {
                  return "employeeId is Required";
                } else {
                  return "";
                }
       
         case "wfh":
                if (!value || value.trim() === "") {
                  return "wfh ans is Required";
                } else {
                  return "";
                }
         case "manager":
                if (!value || value.trim() === "") {
                  return "manager is Required";
                } else {
                  return "";
                }
            
      default: {
        return "";
      }
    }
  };

  const onSubmitClick = (e, formData, onSubmit) => {
    e.preventDefault();
    console.log('onsubmit click');
    let validationErrors = {};
Object.keys(formData).forEach(name => {
  const error = validate(name, formData[name]);
  if (error && error.length > 0) {
    validationErrors[name] = error;
  }
});

if (Object.keys(validationErrors).length > 0) {
  setErrors({ errors: validationErrors });
  return;
}
else{
        onSubmit(formData); 
}     
      };

  return (
    <div className="container ">
      <div className="form-input">
        <h2>Multistep Form</h2>
        <ul className="booking-container-steps">
          <li
            className={step === 1 ? "steps-is-active" : ""}
            onClick={() => {
              setStep(1);
            }}
          >
            <span>Personal Info</span>
          </li>
          <li
            className={step === 2 ? "steps-is-active" : ""}
            onClick={() => {
              setStep(2);
            }}
          >
            <span>Employee Info</span>
          </li>
          <li
            className={step === 3 ? "steps-is-active" : ""}
            onClick={() => {
              setStep(3);
            }}
          >
            <span>Comments</span>
          </li>
          <li
            className={step === 4 ? "steps-is-active" : ""}
            onClick={() => {
              setStep(4);
            }}
          >
            <span>Done</span>
          </li>
        </ul>
        <div className="booking-container-step">
          <Suspense fallback={<h1>Loading....</h1>}>
            {step == 1 && (
              <Step1
                errors={errors}
                setErrors={setErrors}
                validate={validate}
                onSubmit={(formData) => {
                  setStep(2);
                  setStep1FormData(formData);
                }}

                onSubmitClick={onSubmitClick}
                handleChange={(e) => {
                  setStep1FormData((prev) => {
                    return {
                      ...prev,
                      [e.target.name]: e.target.value,
                    };
                  });
                 
                }}
                formData={step1FormData}
              />
            )}
            {step == 2 && (
              <Step2
              errors={errors}
              onSubmitClick={onSubmitClick}
              setErrors={setErrors}
              validate={validate}
                onSubmit={(formData) => {
                  setStep(3);
                  setStep2FormData(formData);
                }}
                onBackClick={() => {
                  setStep(1);
                }}
                handleChange={(e) => {
                  setStep2FormData((prev) => {
                    return {
                      ...prev,
                      [e.target.name]: e.target.value,
                    };
                  });
                  setErrors((prev) =>{
                      return{
                          ...prev,
                          [e.target.name]: validate(e.target.name, e.target.value)
                      }
                  })
                }}
                formData={step2FormData}
              />
            )}
            {step == 3 && (
              <Step3
              errors={errors}
              setErrors={setErrors}
              onSubmitClick={onSubmitClick}
              validate={validate}
                onBackClick={() => {
                  setStep(2);
                }}
                onSubmit={(formData) => {
                  setStep(4);
                  setStep3FormData(formData);
                 
                }}
                handleChange={(e) => {
                  setStep3FormData((prev) => {
                    return {
                      ...prev,
                      [e.target.name]: e.target.value,
                    };
                  });
                }}
                formData={step3FormData}
              />
            )}
            {step == 4 && (
              <Step4
                step1FormData={step1FormData}
                step2FormData={step2FormData}
                step3FormData={step3FormData}
              />
            )}
          </Suspense>
        </div>
      </div>
      <Accordian step1FormData={step1FormData} step2FormData={step2FormData} step3FormData={step3FormData}/>
    </div>
  );
};

export default MainForm;

import React from 'react';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Modal from '@mui/material/Modal';

const Popup = ({step1FormData, step2FormData, step3FormData}) => {
  const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
  };

  const [open, setOpen] = React.useState(false);
  const onSubmitClick = () => {
      setOpen(true)
  };
//   const handleOpen = () => setOpen(true);
const handleClose = () => setOpen(false);

  return (
    <div className='modal-wrapper'>
    <Button className='popup-btn' onClick={onSubmitClick}>View Details</Button>
    <Modal
      open={open}
      onClose={handleClose}
      aria-labelledby="modal-modal-title"
      aria-describedby="modal-modal-description"
    >
      <Box sx={style}>
        <Typography id="modal-modal-title" variant="h6" component="h2">
          Form Details
        </Typography>
        <Typography id="modal-modal-description" sx={{ mt: 2 }}>
              <span className='title'> Personal Info </span> 
              First Name:{step1FormData.fname} <br />
              Last Name:{step1FormData.lname} <br />
              Mobile Number:{step1FormData.number}<br />
              Email:{step1FormData.email}<br />
             <span className="title"> Employee Info</span> 
              Department:{step2FormData.department} <br />
              EmployeeId:{step2FormData.employeeId} <br />
              Role:{step2FormData.role}<br />
              <span className="title">Comments</span> 
              Working From Home:{step3FormData.wfh} <br />
              Manager:{step3FormData.manager} <br />
              Comments:{step3FormData.comment}<br />
        </Typography>
      </Box>
    </Modal>
  </div>
  )
}

export default Popup
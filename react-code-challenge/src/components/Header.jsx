import React from 'react'

const Header = () => {
  return (
    <div className='header-wrapper'>
        <div className="container-fluid header">
            <div className="logo"><img src="https://hiprodcdn.azureedge.net/-/media/Project/Horizontal/DotCom/HorizontalCom/Horizontal-Large.png?h=242&w=472&revision=6de04fce-adac-4583-ae31-f2f40ae567b2&modified=20191012132858&hash=8403D468B430E934838D7BE1915CB8FA" alt="" /></div>
            <div className="brand">Multistep Form</div>
        </div>
    </div>
  )
}

export default Header
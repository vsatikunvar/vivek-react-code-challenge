// import './style/Main.scss'
// import 'bootstrap/dist/css/bootstrap.min.css';
// import 'bootstrap/dist/js/bootstrap.bundle.min';
import MainForm from './components/MainForm';
import Header from './components/Header';

function App() {
  return (
    <div className="App">
      <Header/>
      <MainForm/>
    </div>
  );
}

export default App;
